extends Area2D

export (PackedScene) var next_scene
export var fade_timer_length = 1.0
export var fade_speed = 1.0
export var jumpscare_on_transition = false

onready var transition_scene = preload("res://levels/TransitionScreen/TransitionScreen.tscn")

onready var root = get_node("/root/Root")
onready var player_1 = root.current_scene.get_node("Player1")
onready var player_2 = root.current_scene.get_node("Player2")
onready var screen_effects = root.get_node("ScreenEffects")
onready var soundboard = root.get_node("Soundboard")

var triggered = false


func _on_LevelTransition_body_entered(body):
	if !triggered:
		if body == player_1 || body == player_2:
			triggered = true
			screen_effects.fade_to_black(fade_speed)
			$FadeTimer.start(fade_timer_length)


func _on_FadeTimer_timeout():
	if jumpscare_on_transition:
		soundboard.emit_signal("sfx_jumpscare_3")
	root.next_level = next_scene
	root.load_scene(transition_scene)
