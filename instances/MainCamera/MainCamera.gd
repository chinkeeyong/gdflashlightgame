#####################################################################################################
# MainCamera.gd
# by Chin Kee Yong
#
# This script makes the camera follow the players.
# Should always be placed in the root node named Level alongside Player1 and Player2
#####################################################################################################

extends Camera2D

onready var player_1 = get_node("/root/Root").current_scene.get_node("Player1")
onready var player_2 = get_node("/root/Root").current_scene.get_node("Player2")

var shake_amount = 0.0
const SHAKE_SCALE_FACTOR = 0.0015
const SHAKE_DECAY = 1.0

func _ready():
	smoothing_enabled = false
	position = player_1.position.linear_interpolate(player_2.position, 0.5)
	smoothing_enabled = true

func _process(delta):
	# The camera moves to the midpoint between Player 1 and Player 2
	position = player_1.position.linear_interpolate(player_2.position, 0.5)
	
func _physics_process(delta):
	# Shake the camera if applicable
	if shake_amount > 0:
		var new_zoom = 1.0 + (rand_range(-1.0, 1.0) * shake_amount * SHAKE_SCALE_FACTOR)
		set_zoom(Vector2(new_zoom, new_zoom)) 
		set_offset(Vector2(rand_range(-1.0, 1.0) * shake_amount, rand_range(-1.0, 1.0) * shake_amount))
		shake_amount -= SHAKE_DECAY

func _shake(amount):
	if shake_amount < amount:
		shake_amount = amount