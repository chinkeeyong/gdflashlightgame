#####################################################################################################
# EnemyBoss.gd
# by Chin Kee Yong
#####################################################################################################

extends Area2D

export var active = true # Whether this enemy is "aggroed" and follows the players or not

onready var root = get_node("/root/Root")
onready var player_1 = root.current_scene.get_node("Player1")
onready var player_2 = root.current_scene.get_node("Player2")
onready var main_camera = root.current_scene.get_node("MainCamera")
onready var screen_effects = root.get_node("ScreenEffects")
onready var soundboard = root.get_node("Soundboard")

var speed = 0.0  # How fast the enemy moves
export var starting_speed = 150.0
export var max_speed = 500.0
export var speed_change_cutoff = 1000.0
var movement_vector = Vector2(0.0, 0.0) # Current vector of movement

var in_light_timer = 0 # Set to a grace period when in light. Ticks down every frame we are not in light
const IN_LIGHT_GRACE_FRAMES = 5
signal light_entered # We receive this signal from the players every frame we are in light

var enemy_close_sfx_volume_max = -5 # In volume db
var enemy_close_sfx_volume_dropoff = 20000 # Higher = audible from further away

var enemy_close_screen_shake_magnitude = 75000
var shake_multiplier_when_visible = 5.0

var enemy_close_freakout_magnitude = 1500000 # How red to make the screen when the enemy is nearby

var jumpscare_light_timer = 0 # Set to a grace period when in flashlight. Ticks down every frame we are not in flashlight
var jumpscare_minimum_distance = 550 # How close an enemy has to be in pixels for us to do the jumpscare
var jumpscare_shake_multiplier = 5.0 # How much more to shake the camera when entering light for the first time

const MAX_PERSONAL_LIGHT_ENERGY = 0.7

export (PackedScene) var game_over_screen # The Godot scene this sends you to when it kills you


func _ready():
	add_to_group("enemies")
	$AnimatedSprite.play()
	if !active:
		$AnimatedSprite.visible = false


func _physics_process(delta):
	
	if active:
		
		# Get the distance to the closest player
		var nearest_player_distance = 0.0
		if (global_position.distance_to(player_1.global_position) < global_position.distance_to(player_2.global_position)):
			# Player 1 is nearer
			nearest_player_distance = global_position.distance_squared_to(player_1.global_position)
		else:
			# Player 2 is nearer
			nearest_player_distance = global_position.distance_squared_to(player_2.global_position)
		
		#Change speed depending on how far we are from the nearest player
		if nearest_player_distance != 0:
			speed = lerp(starting_speed, max_speed, nearest_player_distance / speed_change_cutoff)
			print(nearest_player_distance / speed_change_cutoff)
		
		# Move towards the closer player, 1 or 2
		if (global_position.distance_to(player_1.global_position) < global_position.distance_to(player_2.global_position)):
			# Player 1 is nearer
			movement_vector = global_position.direction_to(player_1.global_position)
		else:
			# Player 2 is nearer
			movement_vector = global_position.direction_to(player_2.global_position)
		global_position += movement_vector * speed * delta
		
		in_light_timer -= 1
		jumpscare_light_timer -= 1
		
		# Self light
		$Light2D.energy = rand_range(0.0, MAX_PERSONAL_LIGHT_ENERGY)


func _process(delta):
	if active:
		# Get the distance to the closest player
		var nearest_player_distance = 0.0
		if (global_position.distance_to(player_1.global_position) < global_position.distance_to(player_2.global_position)):
			# Player 1 is nearer
			nearest_player_distance = global_position.distance_squared_to(player_1.global_position)
		else:
			# Player 2 is nearer
			nearest_player_distance = global_position.distance_squared_to(player_2.global_position)
		
		# Increase the volume of "enemy close" sfx based on how close the monster is
		$EnemyCloseSound.set_volume_db(enemy_close_sfx_volume_max - (nearest_player_distance / enemy_close_sfx_volume_dropoff))
		
		# Freak out the screen based on how close the monster is
		screen_effects.freakout(enemy_close_freakout_magnitude / nearest_player_distance)
		
		# Increase the screen shake amount based on how close the monster is
		var added_shake_amount = enemy_close_screen_shake_magnitude / nearest_player_distance
		
		# Increase the shake amount if enemy is visible
		if in_light_timer > 0:
			added_shake_amount *= shake_multiplier_when_visible
		
		# Shake the screen
		main_camera._shake(added_shake_amount)


func _on_light_entered(distance, source):
	in_light_timer = IN_LIGHT_GRACE_FRAMES
	if source == "flashlight":
		if jumpscare_light_timer <= 0 && distance < jumpscare_minimum_distance:
			jumpscare()
		jumpscare_light_timer = IN_LIGHT_GRACE_FRAMES


func jumpscare():
	# Do the jumpscare sound (follows timer in soundboard)
	soundboard.emit_signal("sfx_jumpscare_1")
	
	# Get distance to nearest player
	var nearest_player_distance = 0.0
	if (global_position.distance_to(player_1.global_position) < global_position.distance_to(player_2.global_position)):
		# Player 1 is nearer
		nearest_player_distance = global_position.distance_squared_to(player_1.global_position)
	else:
		# Player 2 is nearer
		nearest_player_distance = global_position.distance_squared_to(player_2.global_position)
	
	# Shake the screen based on how close the monster is
	var added_shake_amount = enemy_close_screen_shake_magnitude / nearest_player_distance * shake_multiplier_when_visible * jumpscare_shake_multiplier
	main_camera._shake(added_shake_amount)


func activate():
	active = true
	$AnimatedSprite.visible = true


func game_over():
	root.load_scene(game_over_screen)

func _on_EnemyBoss_body_entered(body):
	if active:
		if body == player_1 || body == player_2:
			if !body.god_mode:
				game_over()
