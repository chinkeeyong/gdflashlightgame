#####################################################################################################
# TentacleDoor.gd
# by Chin Kee Yong
#
# This is a basic horizontal door with collision. It appears in front of players that are behind it.
# Opens and closes with open() and close(). These functions can be called from signals
#####################################################################################################

extends KinematicBody2D


onready var root = get_node("/root/Root")
onready var player_1 = root.current_scene.get_node("Player1")
onready var player_2 = root.current_scene.get_node("Player2")

export (int, "Closed", "Open") var initially
var open = false

export (PackedScene) var game_over_screen # The Godot scene this sends you to when you stand inside it when it closes


func _ready():
	if initially == 0:
		$AnimatedSprite.frame = 0
		$MiddleCollider.disabled = false
	else:
		$AnimatedSprite.frame = $AnimatedSprite.frames.get_frame_count("default") - 1
		$MiddleCollider.disabled = true


func _physics_process(delta):
	z_index = floor(global_position.y)
	if $AnimatedSprite.frame == 1:
		if !open:
			if $DeathZone.overlaps_body(player_1) || $DeathZone.overlaps_body(player_2):
				game_over()
			$MiddleCollider.disabled = false


func _open():
	if !open:
		open = true
		$MiddleCollider.disabled = true
		$AnimatedSprite.frame = 0
		$AnimatedSprite.play("default")
		$OpeningSFX.play(0.0)


func _close():
	if open:
		open = false
		$AnimatedSprite.frame = $AnimatedSprite.frames.get_frame_count("default") - 1
		$AnimatedSprite.play("default", true)
		$OpeningSFX.play(0.0)


func game_over():
	root.load_scene(game_over_screen)