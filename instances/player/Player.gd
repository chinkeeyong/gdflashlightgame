#####################################################################################################
# Player.gd
# by Chin Kee Yong
#
# This script handles the behavior of the Player node.
# It reads Input and translates that to in-game movement and flashlight operation.
#####################################################################################################

extends KinematicBody2D

export var is_player_2 = false # Whether this is an instance of Player 2 (as opposed to Player 1)
export var normalize_axis_input = true # Whether to correct gamepad axis square input to a circle. For compatibility with certain pads

var movement_input_vector = Vector2(0,0) # The current direction that the player is pressing on left stick
export var speed = 250 # How fast the player will move

var flashlight_input_vector = Vector2(0,0) # The current direction that the player is pressing on right stick
const FLASHLIGHT_TURN_SPEED = 0.5 # How fast flashlight lerps to new position
var previous_flashlight_rotation = Vector2(0,0) # Stores last rotation of flashlight
const FLASHLIGHT_MAX_TURN_DELTA = 0.1 # How much the flashlight can turn before it starts flickering
const FLASHLIGHT_MAX_TURN_DELTA_ALLOWED_FRAMES = 20 # Number of frames you can turn greater than max turn delta before it starts flickering
var flashlight_max_turn_delta_frames = 0

const FLASHLIGHT_MAX_ENERGY = 0.9
const FLASHLIGHT_FLICKER_MAX_ENERGY = 0.9

var flashlight_active = true

const STICK_DEAD_ZONE = 0.01 # The dead zone for joystick axes

var facing_direction = "Front"

var controllable = true

export var god_mode = false


func _ready():
	$FlashlightArm/Flashlight/LightMask.visible = true


func _physics_process(delta):
	
	# Update z index
	z_index = floor(global_position.y)
	
	# Get the player's movement vector and use it as acceleration
	if controllable:
		movement_input_vector = get_stick_input("left")
		move_and_slide(movement_input_vector * speed)
		
		# Game over if we walked into an enemy
		if !god_mode:
			for i in get_slide_count():
				var collision = get_slide_collision(i)
				if collision.collider.is_in_group("enemies"):
					collision.collider.game_over()
	
		# Turn flashlight
		
		var current_flashlight_rotation = Vector2(1.0, 0.0).rotated($FlashlightArm.rotation)
		flashlight_input_vector = get_stick_input("right")
		
		current_flashlight_rotation = current_flashlight_rotation.linear_interpolate(flashlight_input_vector, FLASHLIGHT_TURN_SPEED)
		$FlashlightArm.rotation = current_flashlight_rotation.angle();
		
		# Check if we have been shaking the flashlight too much, flicker flashlight if applicable
		var flashlight_turn_delta = abs(previous_flashlight_rotation.angle_to(current_flashlight_rotation))
		if flashlight_turn_delta > FLASHLIGHT_MAX_TURN_DELTA:
			flashlight_max_turn_delta_frames = clamp(flashlight_max_turn_delta_frames + 1, 0, FLASHLIGHT_MAX_TURN_DELTA_ALLOWED_FRAMES);
		else:
			flashlight_max_turn_delta_frames = clamp(flashlight_max_turn_delta_frames - 1, 0, FLASHLIGHT_MAX_TURN_DELTA_ALLOWED_FRAMES);
		
		if flashlight_max_turn_delta_frames >= FLASHLIGHT_MAX_TURN_DELTA_ALLOWED_FRAMES:
			flashlight_active = false
			$FlashlightArm/Flashlight/Cone/CollisionShape2D.disabled = true
			$FlashlightArm/Flashlight/FlickerTimer.start()
		
		# Save flashlight rotation to compare for next physics frame
		previous_flashlight_rotation = current_flashlight_rotation
		
		# Flicker the flashlight if it's not active, or restore full brightness if it's active
		if flashlight_active:
			$FlashlightArm/Flashlight/LightMask.energy = FLASHLIGHT_MAX_ENERGY
		else:
			$FlashlightArm/Flashlight/LightMask.energy = rand_range(0.0, FLASHLIGHT_FLICKER_MAX_ENERGY)
		
		# Raycast to every enemy in the flashlight cone and check if they are in line of sight
		if flashlight_active:
			var space_state = get_world_2d().direct_space_state
			for raycast_target in $FlashlightArm/Flashlight/Cone.get_overlapping_bodies():
				raycast_target.emit_signal("light_entered", global_position.distance_to(raycast_target.global_position), "flashlight")
				#var result = space_state.intersect_ray($FlashlightArm.global_position, raycast_target.global_position, [], 8)
				#if result:
				#	if result.collider == raycast_target:
				#		raycast_target.emit_signal("light_entered", global_position.distance_to(raycast_target.global_position), "flashlight")
	else:
		movement_input_vector = Vector2(0, 0)

func _process(delta):
	
	# Handle player animation
	
	if !is_player_2:
		
		if flashlight_input_vector.y < 0: # Pointing up
			facing_direction = "Back"
			if movement_input_vector.length() > 0: # Walking animation
				$Sprite/AnimationPlayer.current_animation = "BoyBackWalk"
			else:
				$Sprite/AnimationPlayer.current_animation = "BoyIdleBack"
			
		else: # Pointing down or sideways
			if flashlight_input_vector.length() <= 0:
				pass
			elif abs(flashlight_input_vector.y) > abs(flashlight_input_vector.x): # Vertical animation
				facing_direction = "Front"
			else: # Horizontal animation
				if flashlight_input_vector.x > 0: # Moving right
					facing_direction = "Right"
				else: # Moving left
					facing_direction = "Left"
			
			match facing_direction:
				"Front":
					if movement_input_vector.length() > 0: # Walking animation
						$Sprite/AnimationPlayer.current_animation = "BoyFrontWalk"
					else:
						$Sprite/AnimationPlayer.current_animation = "BoyIdleFront"
				"Right":
					if movement_input_vector.length() > 0: # Walking animation
						$Sprite/AnimationPlayer.current_animation = "BoyRightWalk"
					else:
						$Sprite/AnimationPlayer.current_animation = "BoyIdleRight"
				"Left":
					if movement_input_vector.length() > 0: # Walking animation
						$Sprite/AnimationPlayer.current_animation = "BoyLeftWalk"
					else:
						$Sprite/AnimationPlayer.current_animation = "BoyIdleLeft"
		
	else:
		
		if flashlight_input_vector.y < 0: # Pointing up
			facing_direction = "Back"
			if movement_input_vector.length() > 0: # Walking animation
				$Sprite/AnimationPlayer.current_animation = "GirlWalkBack"
			else:
				$Sprite/AnimationPlayer.current_animation = "GirlIdleBack"
			
		else: # Pointing down or sideways
			if flashlight_input_vector.length() <= 0:
				pass
			elif abs(flashlight_input_vector.y) > abs(flashlight_input_vector.x): # Vertical animation
				facing_direction = "Front"
			else: # Horizontal animation
				if flashlight_input_vector.x > 0: # Moving right
					facing_direction = "Right"
				else: # Moving left
					facing_direction = "Left"
			
			match facing_direction:
				"Front":
					if movement_input_vector.length() > 0: # Walking animation
						$Sprite/AnimationPlayer.current_animation = "GirlWalkFront"
					else:
						$Sprite/AnimationPlayer.current_animation = "GirlIdleFront"
				"Right":
					if movement_input_vector.length() > 0: # Walking animation
						$Sprite/AnimationPlayer.current_animation = "GirlWalkRight"
					else:
						$Sprite/AnimationPlayer.current_animation = "GirlIdleRight"
				"Left":
					if movement_input_vector.length() > 0: # Walking animation
						$Sprite/AnimationPlayer.current_animation = "GirlWalkLeft"
					else:
						$Sprite/AnimationPlayer.current_animation = "GirlIdleLeft"
	
	# Change player sprite depth so flashlight appears behind the character when appropriate
	if facing_direction == "Back":
		$FlashlightArm.z_index = -1
	else:
		$FlashlightArm.z_index = 1

func get_stick_input(stick):
	
	# Get stick input
	var stick_x = 0
	var stick_y = 0
	
	if stick == "left":
		if is_player_2 == false:
			stick_x = Input.get_action_strength("p1_move_right") - Input.get_action_strength("p1_move_left")
			stick_y = Input.get_action_strength("p1_move_down") - Input.get_action_strength("p1_move_up")
		else:
			stick_x = Input.get_action_strength("p2_move_right") - Input.get_action_strength("p2_move_left")
			stick_y = Input.get_action_strength("p2_move_down") - Input.get_action_strength("p2_move_up")
	else:
		if is_player_2 == false:
			stick_x = Input.get_action_strength("p1_look_right") - Input.get_action_strength("p1_look_left")
			stick_y = Input.get_action_strength("p1_look_down") - Input.get_action_strength("p1_look_up")
		else:
			stick_x = Input.get_action_strength("p2_look_right") - Input.get_action_strength("p2_look_left")
			stick_y = Input.get_action_strength("p2_look_down") - Input.get_action_strength("p2_look_up")
	
	if normalize_axis_input:
		# Stick input forms a right triangle. Get length of hypotenuse via Pythagoras' theorem - this gives axis magnitude
		var stick_magnitude = sqrt(pow(stick_x, 2) + pow(stick_y, 2)) 
		if stick_magnitude > 1:
			stick_magnitude = 1
		# We have a dead zone at the center of the stick to prevent old joysticks from drifting slightly in a direction.
		# Here we put a lower limit at the dead zone and re-normalize the magnitude
		var stick_active_range = (stick_magnitude - STICK_DEAD_ZONE) / (1 - STICK_DEAD_ZONE)
		if stick_active_range < 0:
			stick_active_range = 0
		# Get the angle we are pushing the axis in, then use it to make a new stick x and y with our magnitude
		var stick_angle = atan2(stick_y, stick_x)
		var new_stick_x = cos(stick_angle) * stick_active_range
		var new_stick_y = sin(stick_angle) * stick_active_range
		return Vector2(new_stick_x, new_stick_y)
	else:
		return Vector2(stick_x, stick_y)

func _on_FlickerTimer_timeout():
	flashlight_active = true
	$FlashlightArm/Flashlight/Cone/CollisionShape2D.disabled = false
