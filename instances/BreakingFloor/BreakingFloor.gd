extends Area2D


export (PackedScene) var next_scene
export (PackedScene) var transition_scene

onready var root = get_node("/root/Root")
onready var player_1 = root.current_scene.get_node("Player1")
onready var player_2 = root.current_scene.get_node("Player2")
onready var screen_effects = root.get_node("ScreenEffects")
onready var soundboard = root.get_node("Soundboard")

var triggered = false


func _on_body_entered(body):
	if !triggered:
		if body == player_1 || body == player_2:
			triggered = true
			get_tree().paused = true
			$FreezeFrameTimer.start()
			$AnimatedSprite.play("BrokenFloor")
			$sfx_wood_break.play()


func _on_FreezeFrameTimer_timeout():
	$ThudTimer.start()
	screen_effects.fade_to_black(1.5)

func _on_ThudTimer_timeout():
	$FadeTimer.start()
	$sfx_thud.play()

func _on_FadeTimer_timeout():
	get_tree().paused = false
	root.next_level = next_scene
	root.load_scene(transition_scene)
