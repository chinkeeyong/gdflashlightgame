#####################################################################################################
# EnemyBogey.gd
# by Chin Kee Yong
#
# This is a simple test enemy that slowly moves towards the players but stops if inside flashlight cone
#####################################################################################################

extends KinematicBody2D

export var active = false # Whether this enemy is "aggroed" and follows the players or not
export var activate_on_light_entered = false # Whether this enemy aggros when you shine light on it for the first time

onready var root = get_node("/root/Root")
onready var player_1 = root.current_scene.get_node("Player1")
onready var player_2 = root.current_scene.get_node("Player2")
onready var main_camera = root.current_scene.get_node("MainCamera")
onready var screen_effects = root.get_node("ScreenEffects")
onready var soundboard = root.get_node("Soundboard")

export var speed = 90 # How fast the enemy moves
export var run_away_speed = 60 # How fast the enemy moves when running away
var movement_vector = Vector2(0.0, 0.0) # Current vector of movement

var in_light_timer = 0 # Set to a grace period when in light. Ticks down every frame we are not in light
const IN_LIGHT_GRACE_FRAMES = 5
var light_defense_frames = 0 # Ticks down every frame in light. Can't move at stop. Runs away at 0. Resets to max when not in light
export var frames_runaway_in_light = 120
export var frames_moved_in_light = 10
signal light_entered # We receive this signal from the players every frame we are in light

var enemy_close_sfx_volume_max = -5 # In volume db
var enemy_close_sfx_volume_dropoff = 15000 # Higher = audible from further away

var enemy_close_screen_shake_magnitude = 50000
var shake_multiplier_when_visible = 5.0

var enemy_close_freakout_magnitude = 1000000 # How red to make the screen when the enemy is nearby

var jumpscare_light_timer = 0 # Set to a grace period when in flashlight. Ticks down every frame we are not in flashlight
var jumpscare_minimum_distance = 550 # How close an enemy has to be in pixels for us to do the jumpscare
var jumpscare_shake_multiplier = 5.0 # How much more to shake the camera when entering light for the first time

export (PackedScene) var game_over_screen # The Godot scene this sends you to when it kills you


func _ready():
	add_to_group("enemies")
	$AnimatedSprite.animation = "idle"
	$AnimatedSprite.play()


func _physics_process(delta):
	
	# When it's in light, light defense frames drain
	# When it's not in light, light defense frames refill to max
	if in_light_timer <= 0:
		light_defense_frames = frames_runaway_in_light + frames_moved_in_light
	else:
		light_defense_frames -= 1
	
	if active:
		# Do we have enough light defense frames to move?
		if light_defense_frames > frames_runaway_in_light:
			# Move towards the closer player, 1 or 2
			if (global_position.distance_to(player_1.global_position) < global_position.distance_to(player_2.global_position)):
				# Player 1 is nearer
				movement_vector = global_position.direction_to(player_1.global_position)
			else:
				# Player 2 is nearer
				movement_vector = global_position.direction_to(player_2.global_position)
			move_and_slide(movement_vector * speed)
			$AnimatedSprite.animation = "walking"
			# We've been in flashlight long enough to stop moving, but not enough to run away
		elif light_defense_frames > 0:
			$AnimatedSprite.animation = "idle"
			# Run away!
		else:
			# Move away from the closer player, 1 or 2
			if (global_position.distance_to(player_1.global_position) < global_position.distance_to(player_2.global_position)):
				# Player 1 is nearer
				movement_vector = global_position.direction_to(player_1.global_position)
			else:
				# Player 2 is nearer
				movement_vector = global_position.direction_to(player_2.global_position)
			move_and_slide(movement_vector * run_away_speed * -1)
			$AnimatedSprite.animation = "walking_slow"
		
		# Game over the players if we walked into them
		for i in get_slide_count():
			var collision = get_slide_collision(i)
			if collision.collider == player_1 || collision.collider == player_2:
				if !player_1.god_mode && !player_2.god_mode:
					game_over()
		
		in_light_timer -= 1
		jumpscare_light_timer -= 1


func _process(delta):
	# Get the distance to the closest player
	var nearest_player_distance = 0.0
	if (global_position.distance_to(player_1.global_position) < global_position.distance_to(player_2.global_position)):
		# Player 1 is nearer
		nearest_player_distance = global_position.distance_squared_to(player_1.global_position)
	else:
		# Player 2 is nearer
		nearest_player_distance = global_position.distance_squared_to(player_2.global_position)
	
	# Increase the volume of "enemy close" sfx based on how close the monster is
	$EnemyCloseSound.set_volume_db(enemy_close_sfx_volume_max - (nearest_player_distance / enemy_close_sfx_volume_dropoff))
	
	# Freak out the screen based on how close the monster is
	screen_effects.freakout(enemy_close_freakout_magnitude / nearest_player_distance)
	
	# Increase the screen shake amount based on how close the monster is
	var added_shake_amount = enemy_close_screen_shake_magnitude / nearest_player_distance
	
	# Increase the shake amount if enemy is visible
	if in_light_timer > 0:
		added_shake_amount *= shake_multiplier_when_visible
	
	# Shake the screen
	main_camera._shake(added_shake_amount)


func _on_light_entered(distance, source):
	in_light_timer = IN_LIGHT_GRACE_FRAMES
	if activate_on_light_entered && !active:
		active = true
	if source == "flashlight":
		if jumpscare_light_timer <= 0 && distance < jumpscare_minimum_distance:
			jumpscare()
		jumpscare_light_timer = IN_LIGHT_GRACE_FRAMES


func jumpscare():
	# Do the jumpscare sound (follows timer in soundboard)
	soundboard.emit_signal("sfx_jumpscare_1")
	
	# Get distance to nearest player
	var nearest_player_distance = 0.0
	if (global_position.distance_to(player_1.global_position) < global_position.distance_to(player_2.global_position)):
		# Player 1 is nearer
		nearest_player_distance = global_position.distance_squared_to(player_1.global_position)
	else:
		# Player 2 is nearer
		nearest_player_distance = global_position.distance_squared_to(player_2.global_position)
	
	# Shake the screen based on how close the monster is
	var added_shake_amount = enemy_close_screen_shake_magnitude / nearest_player_distance * shake_multiplier_when_visible * jumpscare_shake_multiplier
	main_camera._shake(added_shake_amount)

func game_over():
	root.load_scene(game_over_screen)