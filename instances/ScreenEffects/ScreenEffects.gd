#####################################################################################################
# ScreenEffects.gd
# by Chin Kee Yong
#
# This is a canvas layer which provides several functions for drawing stuff on top of the main game display.
# Blur shader works weirdly with our sprites and lighting so it can only be used in cutscenes
#####################################################################################################

extends CanvasLayer

var fading_to_black = false
var fade_to_black_speed = 0.5

var freakout_amount = 0.0
const FREAKOUT_DECAY = 100.0

onready var vignette_home_position = Vector2(get_viewport().size.x / 2, get_viewport().size.y / 2)
var vignette_sway_timer = 0.0
var vignette_sway_angle = 0.0
var vignette_sway_magnitude = 0.0
const VIGNETTE_SWAY_MAGNITUDE_MAXIMUM = 150.0
const VIGNETTE_SWAY_SPEED = 1
const VIGNETTE_SWAY_ANGULAR_SPEED = 1

var blurring_screen = false
var blur_speed = 0.5
const MAX_BLUR_AMOUNT = 10.0
onready var blur_amount = MAX_BLUR_AMOUNT
var blur_amount_curve = 1.0


func _ready():
	$FadeCurtain.color = Color(0.0, 0.0, 0.0, 1.0)
	
	$Vignette.visible = true
	$Vignette.position = vignette_home_position


func _process(delta):
	
	# Increment or decrement the fade curtain's alpha depending on whether we are fading to black right now
	if fading_to_black:
		var new_alpha = clamp($FadeCurtain.color.a + delta * fade_to_black_speed, 0.0, 1.0)
		$FadeCurtain.color.a = new_alpha
	else:
		var new_alpha = clamp($FadeCurtain.color.a - delta * fade_to_black_speed, 0.0, 1.0)
		$FadeCurtain.color.a = new_alpha
	
	# Blur or unblur the screen.
	# This uses an exponential curve so that blurring/unblurring is faster when blur is higher
	if blurring_screen:
		blur_amount_curve = clamp(blur_amount_curve + (blur_speed * delta), 0.0, 1.0)
		blur_amount = pow(blur_amount_curve, 2) * MAX_BLUR_AMOUNT
	else:
		blur_amount_curve = clamp(blur_amount_curve - (blur_speed * delta), 0.0, 1.0)
		blur_amount = pow(blur_amount_curve, 2) * MAX_BLUR_AMOUNT
	$BlurCurtain.material.set_shader_param("amount", blur_amount)
	
	# Tint the freakout curtain
	if freakout_amount > 0:
		$FreakoutCurtain.color.a8 = rand_range(0, freakout_amount)
		freakout_amount -= FREAKOUT_DECAY * delta
	
	# Increment vignette_sway_timer
	vignette_sway_timer += delta * VIGNETTE_SWAY_SPEED
	if vignette_sway_timer > (PI * 2):
		vignette_sway_timer -= (PI * 2)
	
	# Get vignette_sway_magnitude as a sine function of vignette_sway_timer
	vignette_sway_magnitude = sin(vignette_sway_timer) * VIGNETTE_SWAY_MAGNITUDE_MAXIMUM
	
	# Rotate the swaying vignette
	vignette_sway_angle += randf() * delta * VIGNETTE_SWAY_ANGULAR_SPEED
	
	# Get sway vector
	var vignette_sway_vector = Vector2(vignette_sway_magnitude, 0)
	vignette_sway_vector = vignette_sway_vector.rotated(vignette_sway_angle)
	$Vignette.position = vignette_home_position + vignette_sway_vector


func fade_from_black(new_fade_to_black_speed):
	$FadeCurtain.color.a = 1.0
	fading_to_black = false
	fade_to_black_speed = new_fade_to_black_speed


func fade_to_black(new_fade_to_black_speed):
	$FadeCurtain.color.a = 0.0
	fading_to_black = true
	fade_to_black_speed = new_fade_to_black_speed


func blur(new_blur_speed):
	blur_amount_curve = 0.0
	blurring_screen = true
	blur_speed = new_blur_speed

func unblur(new_blur_speed):
	blur_amount_curve = 1.0
	blurring_screen = false
	blur_speed = new_blur_speed

func reset_blur():
	blur_amount_curve = 0.0
	blurring_screen = false

func freakout(amount):
	if freakout_amount < amount:
		freakout_amount = amount