
# Soundboard.gd
# by Chin Kee Yong
#
# This is a general purpose sound node that plays different sounds when given signals.
#####################################################################################################

extends Node

signal sfx_game_over
signal sfx_jumpscare_0
signal sfx_jumpscare_1
signal sfx_jumpscare_2
signal sfx_jumpscare_3
signal sfx_jumpscare_4

func _on_sfx_game_over():
	$sfx_jumpscare_2.play();

func _on_sfx_jumpscare_0():
	# Only do the jumpscare sound if we didn't do it recently. The timer keeps track of the last time we did it
	if $jumpscare_timer.is_stopped():
		$sfx_jumpscare_0.play();
	$jumpscare_timer.start();

func _on_sfx_jumpscare_1():
	# Only do the jumpscare sound if we didn't do it recently. The timer keeps track of the last time we did it
	if $jumpscare_timer.is_stopped():
		$sfx_jumpscare_1.play();
	$jumpscare_timer.start();

func _on_sfx_jumpscare_2():
	# Only do the jumpscare sound if we didn't do it recently. The timer keeps track of the last time we did it
	if $jumpscare_timer.is_stopped():
		$sfx_jumpscare_2.play();
	$jumpscare_timer.start();

func _on_sfx_jumpscare_3(): # Not actually a jumpscare but just our "end of game" sound now.
	$sfx_jumpscare_3.play();

func _on_sfx_jumpscare_4():
	# Only do the jumpscare sound if we didn't do it recently. The timer keeps track of the last time we did it
	if $jumpscare_timer.is_stopped():
		$sfx_jumpscare_4.play();
	$jumpscare_timer.start();