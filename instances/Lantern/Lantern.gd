#####################################################################################################
# Lantern.gd
# by Chin Kee Yong
#
# This is a generic script used by all lanterns.
# The Mode variable indicates how the lantern behaves and can be changed from the editor per instance
# However exporting the light radius is too much of a pain, so you'll have to edit it manually
# LightMask auto scales to the light radius on ready, so you only have to edit LightRadius
#####################################################################################################

extends StaticBody2D

export (int, "Always On", "Ignite Once", "Ignite and Fizzle") var mode = 0
enum {ALWAYS_ON, IGNITE_ONCE, IGNITE_AND_FIZZLE}

export var time_to_ignite = 2.5
export var time_to_fizzle = 10.0

export var ignited_by_other_lanterns = false

var in_light_timer = 0 # Becomes 5 when in flashlight beam. Ticks down when not in beam.
const IN_LIGHT_GRACE_FRAMES = 5

var on = false
var ignition_percentage = 0.0
var brightness = 0.0
var flicker_amount = 0.4
var lightmask_max_energy = 0.9

var ignited_feedback_timer = 0.0
const IGNITED_FEEDBACK_MAGNITUDE = 0.7
const IGNITED_FEEDBACK_ANIMATION_LENGTH = 2000.0
const IGNITED_FEEDBACK_LERP_AMOUNT = 0.1

onready var ignited_light_radius = $LightRadius.shape_owner_get_shape(0,0).radius

signal lantern_ignited
signal lantern_fizzled
signal light_entered


func _ready():
	$LightMask.visible = true
	$AmbientSFX.volume_db = -80
	if mode == ALWAYS_ON:
		on = true
		ignition_percentage = 1.0
		brightness = 1.0
	update_lightmask()


func _physics_process(delta):
	
	if mode != ALWAYS_ON:
		in_light_timer -= 1
		if in_light_timer <= 0: # Fizzle when not in light
			if !on || mode == IGNITE_AND_FIZZLE:
				if time_to_fizzle == 0: # Avoiding the divide by zero
						fizzle()
				else:
					if ignition_percentage > 0.0:
						ignition_percentage -= delta / time_to_fizzle
						if ignition_percentage < 0.0:
							fizzle()
		elif in_light_timer > 0: # Ignite when in light
			if time_to_ignite == 0: # Avoiding the divide by zero
					ignite()
			else:
				ignition_percentage += delta / time_to_ignite
				if ignition_percentage > 1.0:
					ignite()
		
		# Using the ignition %, get a quadratic curve for the lantern illumination. This looks better than linear
		brightness = 1-pow(1-ignition_percentage, 2)
		
		# Add a bit of flickering if the lantern is powered
		if on || in_light_timer > 0:
			brightness = max(randf() * flicker_amount, brightness)
			brightness = max(randf() * flicker_amount, brightness)
		
		# Clamp brightness to prevent it going below 0 or above our max brightness
		brightness = clamp(brightness, 0, 1)
		
		# Update our light radius and light mask
		$LightRadius.shape_owner_get_shape(0,0).set_radius(ignited_light_radius * brightness)
		update_lightmask()
		if mode == IGNITE_AND_FIZZLE:
			update_sfx()
	
	# Raycast to every enemy in the flashlight cone and check if they are in line of sight
	var space_state = get_world_2d().direct_space_state
	for raycast_target in $LightRadius.get_overlapping_bodies():
		raycast_target.emit_signal("light_entered", global_position.distance_to(raycast_target.global_position), "lantern")
		#var result = space_state.intersect_ray(global_position, raycast_target.global_position, [$LightRadius], 8)
		#if result:
		#	if result.collider == raycast_target:
		#		raycast_target.emit_signal("light_entered", global_position.distance_to(raycast_target.global_position), "lantern")


func _on_light_entered(distance, source):
	if mode != ALWAYS_ON:
		if ignited_by_other_lanterns || source == "flashlight":
			if in_light_timer <= 0:
				if mode == IGNITE_AND_FIZZLE || !on:
					$IgnitionSFX.play(0.0)
			in_light_timer = IN_LIGHT_GRACE_FRAMES


func ignite():
	ignition_percentage = 1.0
	if !on:
		emit_signal("lantern_ignited")
		ignited_feedback_timer = IGNITED_FEEDBACK_ANIMATION_LENGTH
		on = true


func fizzle():
	emit_signal("lantern_fizzled")
	ignition_percentage = 0.0
	on = false


func update_lightmask():
	$LightMask.scale = Vector2(brightness, brightness)
	# Shine a bit brighter for a bit when hitting max brightness
	if ignited_feedback_timer > 0:
		var bonus_brightness = IGNITED_FEEDBACK_MAGNITUDE * ignited_feedback_timer / IGNITED_FEEDBACK_ANIMATION_LENGTH
		$LightMask.energy = clamp(brightness * 3, 0, lightmask_max_energy + bonus_brightness)
		ignited_feedback_timer = lerp(ignited_feedback_timer, 0, IGNITED_FEEDBACK_LERP_AMOUNT)
	else:
		$LightMask.energy = clamp(brightness * 3, 0, lightmask_max_energy)

func update_sfx():
	# Sounds get louder if flickering
	if on && brightness < flicker_amount:
		$AmbientSFX.volume_db = ((brightness / flicker_amount) * -20) + 10
	else:
		$AmbientSFX.volume_db = (brightness - 1) * 30