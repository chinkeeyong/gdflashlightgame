#####################################################################################################
# Root.gd
# by Chin Kee Yong
#
# This is the root node of GDFlashlightGame
# It handles scene management and loading/unloading
#
# Call the function load_level when you are loading a new level. In addition to loading the Godot scene,
# it also flags it as the "checkpoint scene" to go back to after you game over
#####################################################################################################

extends Node

export (PackedScene) var starting_scene

var current_scene
var last_attempted_level
var next_level # This variable is stored here by level exit so it persists between levels. Referenced by TransitionScene.tscn

# Called when the node enters the scene tree for the first time.
func _ready():
	load_level(starting_scene)

func load_level(target_scene):
	if current_scene != null:
		current_scene.queue_free()
	last_attempted_level = target_scene
	current_scene = target_scene.instance()
	add_child(current_scene)

func load_scene(target_scene):
	if current_scene != null:
		current_scene.queue_free()
	current_scene = target_scene.instance()
	add_child(current_scene)