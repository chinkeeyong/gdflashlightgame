extends Node

onready var root = get_node("/root/Root")
onready var screen_effects = root.get_node("ScreenEffects")

func _ready():
	screen_effects.fade_from_black(0.5)
	screen_effects.reset_blur()