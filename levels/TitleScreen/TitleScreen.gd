#####################################################################################################
# TitleScreen.gd
# by Chin Kee Yong
#
# This is a very simple script that just handles the button click and level transition
#####################################################################################################

extends Node

onready var root = get_node("/root/Root/")
onready var screen_effects = get_node("/root/Root/ScreenEffects")

export (PackedScene) var next_scene

var started = false

func _ready():
	screen_effects.fade_from_black(1.0)
	screen_effects.unblur(1.2)

func _unhandled_input(event):
	if event is InputEventJoypadButton || event is InputEventMouseButton || event is InputEventKey:
		if event.pressed && !started && $StartTimer.is_stopped():
				started = true
				$FadeTimer.start()
				screen_effects.fade_to_black(1.0)
				screen_effects.blur(1.2)

func _on_FadeTimer_timeout():
	root.load_level(next_scene)