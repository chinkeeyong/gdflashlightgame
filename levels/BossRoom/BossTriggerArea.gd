extends Area2D

var triggered = false

onready var root = get_node("/root/Root")
onready var player_1 = root.current_scene.get_node("Player1")
onready var player_2 = root.current_scene.get_node("Player2")
onready var screen_effects = root.get_node("ScreenEffects")
onready var soundboard = root.get_node("Soundboard")

signal boss_appears

func _on_BossTrigger_body_entered(body):
	if !triggered:
		if body == player_1 || body == player_2:
			triggered = true
			soundboard._on_sfx_jumpscare_4()
			emit_signal("boss_appears")
