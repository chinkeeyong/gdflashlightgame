#####################################################################################################
# TransitionScreen.gd
# by Chin Kee Yong
#
# This script kicks you to the next level after the timer
#####################################################################################################

extends Node

onready var root = get_node("/root/Root/")
onready var screen_effects = get_node("/root/Root/ScreenEffects")

func _ready():
	$ResetTimer.start()

func _on_ResetTimer_timeout():
	root.load_level(root.next_level)