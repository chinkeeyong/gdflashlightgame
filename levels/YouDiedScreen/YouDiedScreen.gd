#####################################################################################################
# YouDiedScreen.gd
# by Chin Kee Yong
#
# This script plays the jumpscare sound and kicks you back to the last attempted level after the timer
#####################################################################################################

extends Node

onready var root = get_node("/root/Root/")
onready var screen_effects = get_node("/root/Root/ScreenEffects")
onready var soundboard = get_node("/root/Root/Soundboard")

func _ready():
	soundboard.emit_signal("sfx_game_over")
	screen_effects.fade_to_black(0.5)
	$ResetTimer.start()

func _on_ResetTimer_timeout():
	screen_effects.fade_from_black(1.0)
	root.load_level(root.last_attempted_level)
