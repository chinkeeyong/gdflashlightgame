extends Node

onready var screen_effects = get_node("/root/Root/ScreenEffects")
onready var vignette = get_node("/root/Root/ScreenEffects/Vignette")

func _ready():
	screen_effects.fade_from_black(0.2)
	screen_effects.unblur(0.2)
	vignette.visible = false